#Student Name: Sukhwinder Singh
#Student Id: C0736516
// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "InternetButton.h"
#include "math.h"

/*Did you know that the InternetButton can detect if it's moving? It's true!
Specifically it can read when it's being accelerated. Recall that gravity
is a constant acceleration and this becomes very useful- you know the orientation!*/
// int xValue = 0;
// int yValue = 0;
// int zValue = 0;

int intialValue;

int num = 0;
bool numChanged = false;
int jumps = 0;


InternetButton b = InternetButton();

void setup() {
    //Tell b to get everything ready to go
    // Use b.begin(1); if you have the original SparkButton, which does not have a buzzer or a plastic enclosure
    // to use, just add a '1' between the parentheses in the code below.
    b.begin();
    intialValue = b.readZ();
    Particle.function("jumps", checkJumps);
    jumps = 0;

}

void loop(){

    if(numChanged == true){

    //And the z!
     int zValue = b.readZ();
     Particle.publish("check", String(zValue));
     // delay(1000);

    if(zValue > intialValue + 25){
        jumps = jumps + 1;
         delay(1000);
     Particle.publish("check", String(jumps));

    }

    if(jumps <= num/5){
        b.allLedsOn(255, 0, 0);
    }
     else if(jumps > num/5 && jumps <= num/2){
        b.allLedsOn(111, 144, 0);
    }
    else  if(jumps > num/2 && jumps < num - 1){
        b.allLedsOn(55, 200, 0);
    }
    else if(jumps == num){
    b.allLedsOn(0, 255, 0);
    numChanged = false;
    delay(2000);
      b.allLedsOn(0, 0, 0);
    }
    }
}


int checkJumps(String command){
    int how = atoi(command.c_str());

    num = how;

    numChanged = true;

    // In embedded programming, "return 1" means that
    // the function finished successfully
    return 1;
}
